/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*************************************!*\
  !*** ./src/css-test/build/index.js ***!
  \*************************************/
!function () {
  "use strict";

  var e,
      t = {
    273: function () {
      var e = window.wp.blocks,
          t = window.wp.element,
          r = window.wp.i18n,
          n = window.wp.blockEditor;
      (0, e.registerBlockType)("create-block/css-test", {
        edit: function () {
          return (0, t.createElement)("p", (0, n.useBlockProps)(), (0, r.__)("Css Test – hello from the editor!", "css-test"));
        },
        save: function () {
          return (0, t.createElement)("p", n.useBlockProps.save(), (0, r.__)("Css Test – hello from the saved content!", "css-test"));
        }
      });
    }
  },
      r = {};

  function n(e) {
    var o = r[e];
    if (void 0 !== o) return o.exports;
    var s = r[e] = {
      exports: {}
    };
    return t[e](s, s.exports, n), s.exports;
  }

  n.m = t, e = [], n.O = function (t, r, o, s) {
    if (!r) {
      var c = 1 / 0;

      for (l = 0; l < e.length; l++) {
        r = e[l][0], o = e[l][1], s = e[l][2];

        for (var i = !0, u = 0; u < r.length; u++) (!1 & s || c >= s) && Object.keys(n.O).every(function (e) {
          return n.O[e](r[u]);
        }) ? r.splice(u--, 1) : (i = !1, s < c && (c = s));

        if (i) {
          e.splice(l--, 1);
          var f = o();
          void 0 !== f && (t = f);
        }
      }

      return t;
    }

    s = s || 0;

    for (var l = e.length; l > 0 && e[l - 1][2] > s; l--) e[l] = e[l - 1];

    e[l] = [r, o, s];
  }, n.o = function (e, t) {
    return Object.prototype.hasOwnProperty.call(e, t);
  }, function () {
    var e = {
      826: 0,
      431: 0
    };

    n.O.j = function (t) {
      return 0 === e[t];
    };

    var t = function (t, r) {
      var o,
          s,
          c = r[0],
          i = r[1],
          u = r[2],
          f = 0;

      if (c.some(function (t) {
        return 0 !== e[t];
      })) {
        for (o in i) n.o(i, o) && (n.m[o] = i[o]);

        if (u) var l = u(n);
      }

      for (t && t(r); f < c.length; f++) s = c[f], n.o(e, s) && e[s] && e[s][0](), e[s] = 0;

      return n.O(l);
    },
        r = self.webpackChunkcss_test = self.webpackChunkcss_test || [];

    r.forEach(t.bind(null, 0)), r.push = t.bind(null, r.push.bind(r));
  }();
  var o = n.O(void 0, [431], function () {
    return n(273);
  });
  o = n.O(o);
}();
/******/ })()
;
//# sourceMappingURL=index.js.map