<?php
/**
 * Plugin Name:       Gdk Blocks
 * Description:       Example block written with ESNext standard and JSX support – build step required.
 * Requires at least: 5.8
 * Requires PHP:      7.0
 * Version:           0.1.0
 * Author:            The WordPress Contributors
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       gdk-blocks
 *
 * @package           create-block
 */

/**
 * Registers the block using the metadata loaded from the `block.json` file.
 * Behind the scenes, it registers also all assets so they can be enqueued
 * through the block editor in the corresponding context.
 *
 * @see https://developer.wordpress.org/reference/functions/register_block_type/
 */
function create_block_gdk_blocks_block_init() {
	register_block_type( __DIR__ . '/build/1-link-btn' );
	register_block_type( __DIR__ . '/build/2-post-taxo' );

}
add_action( 'init', 'create_block_gdk_blocks_block_init' );

/**
 * 
 * Add custom block category
 * 
 * @see https://capitainewp.io/formations/wordpress-creer-blocs-gutenberg/declarer-bloc-gutenberg
 * 
 */
function add_custom_category( $categories ) {
    $categories[] = [
    	'slug' => 'gdk-blocks',
    	'title' => 'Blocks GDK',
    ];

    return $categories;
}
add_filter( 'block_categories_all', 'add_custom_category' );
